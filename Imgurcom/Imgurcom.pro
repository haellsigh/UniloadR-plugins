#-------------------------------------------------
#
# Project created by QtCreator 2015-02-16T16:53:50
#
#-------------------------------------------------

CONFIG      += plugin
QT          += network
TARGET      = Imgurcom
TEMPLATE    = lib

#include "../../UniloadR/uploadinterface.h"
INCLUDEPATH += ../../UniloadR

SOURCES     += Imgurcom.cpp
HEADERS     += Imgurcom.h

DESTDIR     = ../../build-UniloadR-Desktop_Qt_5_2_1_MinGW_32bit-Debug/plugins

OTHER_FILES += \
    Imgurcom.json
