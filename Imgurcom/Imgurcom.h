#ifndef IMGURCOM_H
#define IMGURCOM_H

#include <QObject>
#include <QtNetwork>
#include <QFile>
#include <QMimeDatabase>
#include <QJsonDocument>
#include <QJsonObject>

#include <QtPlugin>
#include "UploaderInterface.h"

class ImgurcomPlugin : public QObject, public UploaderInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.UniloadR.UploaderInterface/1.0" FILE "Imgurcom.json")
    Q_INTERFACES(UploaderInterface)

public:
    void uploadFile(QString const& filePath) Q_DECL_OVERRIDE;
    //void uploadFile(QFile* file) Q_DECL_OVERRIDE;

    QString getSiteName() const Q_DECL_OVERRIDE;
    QString getSiteUrl() const Q_DECL_OVERRIDE;
    QString getSiteDescription() const Q_DECL_OVERRIDE;

public slots:
    void finishedReply();

signals:
    void textError(QString error);
    void uploadError(QNetworkReply::NetworkError error);
    void uploadFinished(QString fileUrl);
    void uploadProgress(qint64 sentBytes, qint64 totalBytes);

private:
    QNetworkAccessManager *m_manager;
    QNetworkReply *m_reply;
    QHttpMultiPart *m_multiPart;
    QFile *m_file;
};

#endif // IMGURCOM_H
