#include "pomfse.h"

#include <QDebug>

QString PomfsePlugin::getSiteName() const
{
    return "Pomf.se";
}

QString PomfsePlugin::getSiteUrl() const
{
    return "http://pomf.se/";
}

QString PomfsePlugin::getSiteDescription() const
{
    return "Pomf.se can host files under 50MiB.\nIt allows any filetype and run under Sweden's law. It does not store any logs of uploads/downlods. Visit their website to donate for the hosting bills.";
}

void PomfsePlugin::uploadFile(QString const& filePath)
{
    qDebug() << "Opening file: " << filePath;
    m_file = new QFile(filePath);

    qDebug() << "Starting upload...";
    if(!m_file->exists()) {
        emit textError("The file doesn't exists."); qDebug() << "Upload error!"; return; }
    if(m_file->size() > 52400000) {
        emit textError("The file is over 50MiB."); qDebug() << "Upload error!"; return; }

    m_multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart filePart;
    //Open the file
    m_file->open(QIODevice::ReadWrite);
    //Set the HttpPart's header
    QMimeDatabase db;
    filePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"files[]\"; filename=\"" + m_file->fileName() + "\""));
    filePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant(db.mimeTypeForData(m_file).name()));
    //Set the HttpPart's body
    filePart.setBodyDevice(m_file);
    //Will delete the m_file along with the multiPart, later
    m_file->setParent(m_multiPart);

    m_multiPart->append(filePart);

    QNetworkRequest request(QUrl("http://pomf.se/upload.php"));
    m_manager = new QNetworkAccessManager();
    m_reply = m_manager->post(request, m_multiPart);

    qDebug() << db.mimeTypeForData(m_file).name();
    //Will delete the m_multiPart along with the m_reply, later
    m_multiPart->setParent(m_reply);
    m_reply->setParent(m_manager);

    //connect(m_reply, &QNetworkReply::finished, "finishedReply");
    connect(m_reply, SIGNAL(finished()),
            this,    SLOT(finishedReply()));

    connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)),
            this,    SIGNAL(uploadError(QNetworkReply::NetworkError)));

    connect(m_reply, SIGNAL(uploadProgress(qint64,qint64)),
            this,    SIGNAL(uploadProgress(qint64,qint64)));

    qDebug() << "Upload started.";
}

void PomfsePlugin::finishedReply()
{
    qDebug() << "Upload finished.";
    QByteArray response = m_reply->readAll();
    qDebug() << "Response: " << response;
    QJsonObject replyObject = QJsonDocument::fromJson(response).object();
    if(!replyObject.value("success").toBool())
    {
        emit textError(tr("Error during the upload: ") + replyObject.value("error").toString());
        qDebug() << "Upload error!";
        return;
    }
    QString fileUrl = "http://a.pomf.se/";
    fileUrl.append(replyObject.value("files").toArray().first().toObject().value("url").toString());

    emit uploadFinished(fileUrl);
}
