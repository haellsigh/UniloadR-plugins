#-------------------------------------------------
#
# Project created by QtCreator 2015-02-15T18:11:35
#
#-------------------------------------------------

CONFIG      += plugin
QT          += network
TARGET      = Pomfse
TEMPLATE    = lib

#include "../../UniloadR/uploadinterface.h"
INCLUDEPATH += ../../UniloadR

SOURCES     += pomfse.cpp
HEADERS     += pomfse.h

DESTDIR     = ../../build-UniloadR-Desktop_Qt_5_2_1_MinGW_32bit-Debug/plugins

OTHER_FILES += \
    Pomfse.json
