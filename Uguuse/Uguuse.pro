CONFIG      += plugin
QT          += network
TARGET      = Uguuse
TEMPLATE    = lib

#for the #include "../../UniloadR/uploadinterface.h"
INCLUDEPATH += ../../UniloadR

SOURCES     += uguuse.cpp
HEADERS     += uguuse.h

DESTDIR     = ../../build-UniloadR-Desktop_Qt_5_2_1_MinGW_32bit-Debug/plugins

OTHER_FILES += \
    Uguuse.json
